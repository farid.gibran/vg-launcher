package id.co.vostra.vanguard.launcher.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

import id.co.vostra.vanguard.launcher.MainActivity;
import id.co.vostra.vanguard.launcher.R;
import id.co.vostra.vanguard.launcher.model.AppsModel;

public class PageAdapter extends PagerAdapter {

    private Context mContext;
    private int[] pages = new int[] {1,2,3};
    private LayoutInflater inflater;
    private AppsModel[] models;

    private int row;
    private int column;
    //private int page = countPage(3, 2, MainActivity.models.size());
    private ArrayList<ArrayList<AppsModel>> theModels;

    public PageAdapter(Context mContext, int row, int column, ArrayList<ArrayList<AppsModel>> theModels) {
        this.mContext = mContext;
        this.row = row;
        this.column = column;
        this.theModels = theModels;
    }

    @Override
    public int getCount() {
        return countPage(row, column, MainActivity.models.size());
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (inflater == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        View itemView = inflater.inflate(R.layout.grid_view, container, false);

        GridView gridView = itemView.findViewById(R.id.gridView);
        GridAdapter adapter = new GridAdapter(mContext, theModels.get(position));
        gridView.setAdapter(adapter);
        gridView.setNumColumns(column);

        container.addView(itemView, 0);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public int countPage(int row, int column, int size) {
        int extra = (size%(row*column))>0? 1 : 0;
        int count = (size/(row*column)) + extra;
        return count;
    }
}
