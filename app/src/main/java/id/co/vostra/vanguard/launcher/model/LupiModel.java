package id.co.vostra.vanguard.launcher.model;

public class LupiModel {
    private CustomApplicationLayoutModel customApplicationLayoutSettings;

    public CustomApplicationLayoutModel getCustomApplicationLayoutSettings() {
        return customApplicationLayoutSettings;
    }

    public void setCustomApplicationLayoutSettings(CustomApplicationLayoutModel customApplicationLayoutSettings) {
        this.customApplicationLayoutSettings = customApplicationLayoutSettings;
    }
}
