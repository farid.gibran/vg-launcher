package id.co.vostra.vanguard.launcher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.co.vostra.vanguard.launcher.adapter.GridAdapter;
import id.co.vostra.vanguard.launcher.adapter.PageAdapter;
import id.co.vostra.vanguard.launcher.model.AppsModel;
import id.co.vostra.vanguard.launcher.model.CustomApplicationLayoutModel;
import id.co.vostra.vanguard.launcher.model.LauncherModel;

public class MainActivity extends AppCompatActivity {

    public static ArrayList<AppsModel> models;
    public static ArrayList<AppsModel> dumModels;
    public static ArrayList<ArrayList<AppsModel>> theModels;
    public static ArrayList<ArrayList<AppsModel>> theDumModels;

    private int row = 3;
    private int column = 3;

    private PackageManager mPackageManager;
    private AppsModel model;
    private AppsModel dumModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPackageManager = getPackageManager();
        models = new ArrayList<>();
        dumModels = new ArrayList<>();

        CustomApplicationLayoutModel layoutModel = new CustomApplicationLayoutModel();
        ArrayList<ArrayList<String>> screens = new ArrayList<>();

        ArrayList<String> screen1 = new ArrayList<>();
        screen1.add("com.coinbase.android"); //1
        screen1.add(""); //2
        screen1.add("com.drop.mynotes.note"); //3
        screen1.add("com.samsung.android.messaging"); //4
        screen1.add(""); //5
        screen1.add("com.samsung.android.app.contacts"); //6
        screen1.add(""); //7
        screen1.add("com.samsung.android.dialer"); //8
        screen1.add("com.android.vending"); //9

        ArrayList<String> screen2 = new ArrayList<>();
        screen2.add(""); //1
        screen2.add(""); //2
        screen2.add("com.willme.topactivity"); //3
        screen2.add(""); //4
        screen2.add(""); //5
        screen2.add(""); //6
        screen2.add(""); //7
        screen2.add(""); //8
        screen2.add(""); //9

        screens.add(screen1);
        screens.add(screen2);

        layoutModel.setRow(3);
        layoutModel.setColumn(3);
        layoutModel.setApplication_list(screens);

        row = layoutModel.getRow();
        column = layoutModel.getColumn();

        List<ResolveInfo> resolveInfos = getAllLauncherIntentResolversSorted();
        List<ApplicationInfo> applicationInfos = getAllInstalledApps();

//        for (int i=0; i<resolveInfos.size(); i++) {
//            dumModel = new AppsModel();
//            dumModel.setLabel((String) resolveInfos.get(i).loadLabel(mPackageManager));
//            dumModel.setIcon(resolveInfos.get(i).loadIcon(mPackageManager));
//            dumModel.setPackageName(resolveInfos.get(i).activityInfo.packageName);
//
//            dumModels.add(model);
//        }

        for (int i=0; i<resolveInfos.size(); i++) {
            model = new AppsModel();
            model.setLabel((String) resolveInfos.get(i).loadLabel(mPackageManager));
            model.setIcon(resolveInfos.get(i).loadIcon(mPackageManager));
            model.setPackageName(resolveInfos.get(i).activityInfo.packageName);
            models.add(model);
        }

        for (int i=0; i<layoutModel.getApplication_list().size(); i++) {
            for (int j=0; j<layoutModel.getApplication_list().get(i).size(); j++) {
                String packageFromLayout = layoutModel.getApplication_list().get(i).get(j);
                if (!packageFromLayout.equals("")) {
                    for (int k = 0; k < models.size(); k++) {
                        if (models.get(k).getPackageName().equals(layoutModel.getApplication_list().get(i).get(j))) {
                            dumModel = new AppsModel();
                            dumModel.setLabel(models.get(k).getLabel());
                            dumModel.setIcon(models.get(k).getIcon());
                            dumModel.setPackageName(models.get(k).getPackageName());
                            dumModels.add(dumModel);
                        }
                    }
                } else {
                    dumModel = new AppsModel();
                    dumModel.setLabel(null);
                    dumModel.setIcon(null);
                    dumModel.setPackageName(null);
                    dumModels.add(dumModel);
                }
            }
        }

        for (int i=0; i<dumModels.size(); i++) {
            if (dumModels.get(i).getLabel() == null)
                Log.i("LOL", "null");
            else
                Log.i("LOL", dumModels.get(i).getLabel());
        }


        theDumModels = new ArrayList<>();
        int j = 0;
        while (j<dumModels.size()) {
            ArrayList<AppsModel> temp = new ArrayList<>();
            while (temp.size()<(row*column)) {
                if (j==dumModels.size()-1) {
                    j++;
                    break;
                } else {
                    temp.add(dumModels.get(j));
                    j++;
                }
            }
            theDumModels.add(temp);
        }

        theModels = new ArrayList<>();
        int i = 0;

        while (i<models.size()) {
            ArrayList<AppsModel> temp = new ArrayList<>();
            while (temp.size()<(row*column)) {
                if (i==models.size()-1) {
                    i++;
                    break;
                } else {
                    i++;
                    temp.add(models.get(i));
                }
            }
            theModels.add(temp);
        }

        ViewPager viewPager = findViewById(R.id.viewPager);
        //PageAdapter pageAdapter = new PageAdapter(this, row, column, theModels);
        PageAdapter pageAdapter = new PageAdapter(this, row, column, theDumModels);
        viewPager.setAdapter(pageAdapter);
    }

    private List<ResolveInfo> getAllLauncherIntentResolversSorted() {
        final Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
        launcherIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> launcherIntentResolvers = mPackageManager.queryIntentActivities(launcherIntent, PackageManager.MATCH_ALL);
        Collections.sort(launcherIntentResolvers, new ResolveInfo.DisplayNameComparator(mPackageManager));

        return launcherIntentResolvers;
    }

    private List<ApplicationInfo> getAllInstalledApps() {
        List<ApplicationInfo> packages = mPackageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        return packages;
    }

    @Override
    public void onBackPressed() {
        //just do nothing
        //super.onBackPressed();
    }
}