package id.co.vostra.vanguard.launcher.model;

import java.util.ArrayList;

public class CustomApplicationLayoutModel {
    private int row;
    private int column;
    private ArrayList<ArrayList<String>> application_list;

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public ArrayList<ArrayList<String>> getApplication_list() {
        return application_list;
    }

    public void setApplication_list(ArrayList<ArrayList<String>> application_list) {
        this.application_list = application_list;
    }
}
