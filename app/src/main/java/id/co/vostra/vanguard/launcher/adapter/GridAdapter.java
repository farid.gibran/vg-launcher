package id.co.vostra.vanguard.launcher.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.vostra.vanguard.launcher.R;
import id.co.vostra.vanguard.launcher.model.AppsModel;

public class GridAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<AppsModel> theModels;

    public GridAdapter(Context context, ArrayList<AppsModel> theModels) {
        this.mContext = context;
        this.theModels = theModels;
    }

    @Override
    public int getCount() {
        return theModels.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.apps_card, null);
        }

        ImageView imageView = convertView.findViewById(R.id.iv_appCard);
        TextView textView = convertView.findViewById(R.id.tv_appCard);

        if (theModels.get(position).getPackageName() == null) {
            return convertView;
        }

        imageView.setImageDrawable(theModels.get(position).getIcon());
        textView.setText(theModels.get(position).getLabel());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("DEB", "CARD CLICKED : " + theModels.get(position).getPackageName());
                Intent launchIntent = mContext.getPackageManager().getLaunchIntentForPackage(theModels.get(position).getPackageName());
                mContext.startActivity(launchIntent);
            }
        });

        return convertView;
    }
}
