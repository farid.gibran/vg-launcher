package id.co.vostra.vanguard.launcher.model;

import java.util.ArrayList;

public class LauncherModel {
    private ArrayList<String> application_list;
    private int column;
    private int row;

    public LauncherModel(ArrayList<String> application_list, int column, int row) {
        this.application_list = application_list;
        this.column = column;
        this.row = row;
    }

    public LauncherModel() {
    }

    public ArrayList<String> getApplication_list() {
        return application_list;
    }

    public void setApplication_list(ArrayList<String> application_list) {
        this.application_list = application_list;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
